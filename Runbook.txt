GitLab Configuration and Development

1. Clone the following GitLab repository.
    https://gitlab.com/pamodyaJayangani/cloud-computing-cw

2. Navigate to the cloned project folder.

3. Excecute the following comand to install required node modules.
    npm install

4. Excecute the following command to run the application locally.
    npm start

5. When project modifications commit into the main branch, pipline will auto triggered. 
